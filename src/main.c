// artix-checkupdates
//
// Copyright (C) 2023 Artix Dev team <artix-dev@artixlinux.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <alpm.h>
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>

#include "utils.h"

#define ALPM_ERROR(h) alpm_strerror(alpm_errno(h))

static const char help_message[] =
    "Usage: artix-checkupdates [options]\n"
    "-a,--artix            Show packages that are only in artix.\n"
    "-b,--blacklist        Repos to skip when listing upgrades (comma-delineated).\n"
    "-d,--downgrades       Show package downgrades.\n"
    "-f,--filter           Filter output on packages present in artix tree directory.\n"
    "-h,--help             Show this help message.\n"
    "-l,--loose-moves      Consider higher versions in Arch as a package move.\n"
    "-m,--moves            Show package moves.\n"
    "-n,--no-sync          Don't sync package databases.\n"
    "-p,--packagers        Packagers to whitelist when showing output (comma-delineated).\n"
    "-u,--upgrades         Show package upgrades.\n"
    "-w,--write <file>     Write artixpkg add/move commands in a bash script.\n"
    "-y,--yaml <file>      Dump package upgrade information to yaml file.\n";

static const struct option long_options[] = {
    {"artix",       no_argument,       0, 'a'},
    {"blacklist",   required_argument, 0, 'b'},
    {"downgrades",  no_argument,       0, 'd'},
    {"filter",      no_argument,       0, 'f'},
    {"help",        no_argument,       0, 'h'},
    {"loose-moves", no_argument,       0, 'l'},
    {"moves",       no_argument,       0, 'm'},
    {"no-sync",     no_argument,       0, 'n'},
    {"packagers",   required_argument, 0, 'p'},
    {"upgrades",    no_argument,       0, 'u'},
    {"write",       required_argument, 0, 'w'},
    {"yaml",        required_argument, 0, 'y'},
    {0,             0,                 0, 0  },
};

struct global {
    bool artix_packages;
    bool no_sync;
    bool package_downgrades;
    bool package_moves;
    bool package_loose_moves;
    bool package_upgrades;
    bool filter;
    bool output;

    FILE *script;
    FILE *yaml;

    struct str_arr *artix_repos;
    struct str_arr *arch_repos;
    struct str_arr *packagers;
    struct str_arr *repo_blacklist;
    char artix_tree_dir[512];

    alpm_handle_t *artix_h;
    alpm_handle_t *arch_h;

    alpm_list_t **write_moves;
    alpm_list_t **write_upgrades;
    int write_moves_lists;
    int write_upgrades_lists;
};


static char dbpath[512];

struct map {
    const char *artix;
    const char *arch;
};

static const struct map repo_map[] = {
    {"system",          "core"            },
    {"world",           "extra"           },
    {"lib32",           "multilib"        },

    {"system-gremlins", "core-testing"    },
    {"world-gremlins",  "extra-testing"   },
    {"lib32-gremlins",  "multilib-testing"},

    {"system-goblins",  "core-staging"    },
    {"world-goblins",   "extra-staging"   },
    {"lib32-goblins",   "multilib-staging"},

 // Generic repo types
    {"gremlins",        "testing"         },
    {"goblins",         "staging"         },

    {NULL,              NULL              },
};

static const struct map pkg_map[] = {
    {"archlinux-mirrorlist",          "pacman-mirrorlist"},
    {"artix-rebuild-order",           "arch-rebuild-order"},
    {"udev",                          "systemd"},
    {"virtualbox-host-modules-artix", "virtualbox-host-modules-arch"},
    {NULL,                            NULL                          },
};

struct ver_map {
    const char *pkgbase;
    const char *find;
    const char *replace;
};

static const struct ver_map pkgver_map[] = {
    {"linux", "arch", "artix"},
    {"linux-rt", "arch", "artix"},
    {"linux-rt-lts", "arch", "artix"},
    {NULL, NULL, NULL},
};

static inline bool
is_pkg_in_tree(const char *tree, const char *pkgbase);
__attribute_warn_unused_result__ static inline alpm_list_t *
list_pkgs(alpm_handle_t *h);
__attribute_warn_unused_result__ static inline alpm_list_t *
list_pkgs_suffix(alpm_handle_t *restrict h, const char *restrict suffix);
static bool
matching_packager(struct str_arr *packagers, const char *packager);
static inline void
print_formatted_package(alpm_pkg_t *restrict archpkg, alpm_pkg_t *restrict artixpkg);
static bool
same_repo(alpm_pkg_t *archpkg, alpm_pkg_t *artixpkg);
static inline void
write_move(struct global *g, alpm_pkg_t *archpkg, alpm_pkg_t *artixpkg);
static inline void
write_upgrade(struct global *g, alpm_pkg_t *archpkg, alpm_pkg_t *artixpkg);

static void
add_to_move_lists(struct global *g, const char *src, char *dst, const char *pkgbase)
{
    for (int i = 0; i < g->write_moves_lists; ++i) {
        // First two elements are always src and dst.
        const char *list_src = alpm_list_nth(g->write_moves[i], 0)->data;
        const char *list_dst = alpm_list_nth(g->write_moves[i], 1)->data;
        if (!strcmp(src, list_src) && !strcmp(dst, list_dst)) {
            alpm_list_append_strdup(&g->write_moves[i], pkgbase);
            return;
        }
    }
    // We ran out of lists. Realloc and add it to a new one.
    int index = g->write_moves_lists;
    ++g->write_moves_lists;
    g->write_moves =
        (alpm_list_t **)realloc(g->write_moves, g->write_moves_lists * sizeof(alpm_list_t));
    alpm_list_t *r = NULL;
    alpm_list_append_strdup(&r, src);
    alpm_list_append_strdup(&r, dst);
    alpm_list_append_strdup(&r, pkgbase);
    g->write_moves[index] = r;
}

static void
add_to_upgrades_lists(struct global *g, const char *dst, const char *pkgbase, char *version)
{
    struct str_arr *pkg_entry = create_str_arr(2, pkgbase, version);
    free(version);

    for (int i = 0; i < g->write_upgrades_lists; ++i) {
        // First element is always the dst.
        struct str_arr *dst_arr = alpm_list_nth(g->write_upgrades[i], 0)->data;
        if (!strcmp(dst, dst_arr->arr[0])) {
            alpm_list_append(&g->write_upgrades[i], pkg_entry);
            return;
        }
    }
    // We ran out of lists. Realloc and add it to a new one.
    int index = g->write_upgrades_lists;
    struct str_arr *dst_entry = create_str_arr(1, dst);
    ++g->write_upgrades_lists;
    g->write_upgrades =
        (alpm_list_t **)realloc(g->write_upgrades, g->write_upgrades_lists * sizeof(alpm_list_t));
    alpm_list_t *r = NULL;
    alpm_list_append(&r, dst_entry);
    alpm_list_append(&r, pkg_entry);
    g->write_upgrades[index] = r;
}

__attribute_warn_unused_result__ static inline const char *
arch2artix(const char *archname)
{
    for (int i = 0; repo_map[i].arch; ++i)
        if (strcmp(repo_map[i].arch, archname) == 0)
            return repo_map[i].artix;
    return "";
}

__attribute_warn_unused_result__ static inline const char *
artix2arch(const char *artixname)
{
    for (int i = 0; repo_map[i].artix; ++i)
        if (strcmp(repo_map[i].artix, artixname) == 0)
            return repo_map[i].arch;
    return "";
}

__attribute_warn_unused_result__ static inline const char *
artix2archpkg(const char *artixname)
{
    for (int i = 0; pkg_map[i].artix; ++i)
        if (strcmp(pkg_map[i].artix, artixname) == 0)
            return pkg_map[i].arch;
    return artixname;
}

static bool
check_repo_blacklist(char *repo, const char *dbname)
{
    // First check for an exact match.
    if (strcmp(repo, dbname) == 0)
        return true;

    // Try the mapped name next.
    if (strcmp(artix2arch(repo), dbname) == 0)
        return true;

    // Check if it matches the dbname suffix
    const char *db_suffix = strrchr(dbname, '-');
    if (!db_suffix)
        return false;

    if (strcmp(repo, db_suffix + 1) == 0)
        return true;

    // Finally try mapping the name.
    if (strcmp(artix2arch(repo), db_suffix + 1) == 0)
        return true;

    return false;
}

__attribute_warn_unused_result__ static inline alpm_handle_t *
create_handle(const char *path)
{
    alpm_errno_t e   = 0;
    alpm_handle_t *h = alpm_initialize(path, path, &e);
    if (!h)
        fprintf(stderr, "Failed initializing alpm: %s\n", alpm_strerror(e));
    return h;
}

// Must be freed.
__attribute_warn_unused_result__ static inline alpm_pkg_t **
get_arch_pkgs(struct global *g, alpm_pkg_t *artixpkg)
{
    alpm_pkg_t **pkgs   = zalloc(sizeof(alpm_pkg_t *) * g->arch_repos->len);
    const char *pkgbase = artix2archpkg(alpm_pkg_get_base(artixpkg));
    const char *pkgname = artix2archpkg(alpm_pkg_get_name(artixpkg));

    int index = 0;
    for (alpm_list_t *e = alpm_get_syncdbs(g->arch_h); e; e = e->next) {
        alpm_pkg_t *pkg = alpm_db_get_pkg(e->data, pkgbase);
        // Fallback to pkgname if pkgbase doesn't return anything.
        if (!pkg)
            pkg = alpm_db_get_pkg(e->data, pkgname);
        const char *dbname = alpm_db_get_name(e->data);
        bool skip          = false;
        if (g->repo_blacklist) {
            for (size_t i = 0; i < g->repo_blacklist->len; ++i) {
                if (check_repo_blacklist(g->repo_blacklist->arr[i], dbname))
                    skip = true;
            }
            if (skip)
                continue;
        }
        if (pkg) {
            pkgs[index] = pkg;
            ++index;
        }
    }
    return pkgs;
}

__attribute_warn_unused_result__ static inline alpm_pkg_t *
get_arch_pkg_suffix(alpm_handle_t *h, alpm_pkg_t *artixpkg, const char *suffix)
{
    const char *pkgbase = artix2archpkg(alpm_pkg_get_base(artixpkg));
    const char *pkgname = artix2archpkg(alpm_pkg_get_name(artixpkg));

    for (alpm_list_t *e = alpm_get_syncdbs(h); e; e = e->next) {
        alpm_db_t *db = e->data;

        const char *dbname = alpm_db_get_name(db);
        const char *p      = strrchr(dbname, '-');

        if (!p) {
            if (suffix)
                continue;
        } else if (!suffix)
            continue;
        else if (strcmp(p + 1, suffix))
            continue;

        alpm_pkg_t *pkg = alpm_db_get_pkg(db, pkgbase);
        // Fallback to pkgname if pkgbase doesn't return anything.
        if (!pkg)
            pkg = alpm_db_get_pkg(db, pkgname);
        if (pkg)
            return pkg;
    }
    return NULL;
}

static void
global_destroy(struct global *g)
{
    alpm_release(g->artix_h);
    alpm_release(g->arch_h);

    if (g->output) {
        for (int i = 0; i < g->write_moves_lists; ++i) {
            for (alpm_list_t *moves = g->write_moves[i]; moves; moves = moves->next)
                free(moves->data);
            alpm_list_free(g->write_moves[i]);
        }
        free(g->write_moves);
        for (int i = 0; i < g->write_upgrades_lists; ++i) {
            for (alpm_list_t *upgrades = g->write_upgrades[i]; upgrades; upgrades = upgrades->next)
                destroy_str_arr(upgrades->data);
            alpm_list_free(g->write_upgrades[i]);
        }
        free(g->write_upgrades);
    }

    if (g->script)
        fclose(g->script);

    if (g->yaml)
        fclose(g->yaml);

    if (g->artix_repos)
        destroy_str_arr(g->artix_repos);
    if (g->arch_repos)
        destroy_str_arr(g->arch_repos);
    if (g->packagers)
        destroy_str_arr(g->packagers);
    if (g->repo_blacklist)
        destroy_str_arr(g->repo_blacklist);

    free(g);
}

static bool
have_operation(struct global *g)
{
    return g->artix_packages || g->package_downgrades || g->package_moves || g->package_upgrades;
}

static inline void
list_add_pkgs(alpm_handle_t *h, alpm_db_t *db, alpm_list_t **l)
{
    alpm_list_t *t = alpm_db_get_pkgcache(db);
    if (!t) {
        if (alpm_errno(h) == ALPM_ERR_OK)
            return;
        fprintf(stderr, "Failed to list packages: %s\n", ALPM_ERROR(h));
        return;
    }

    for (; t; t = t->next)
        *l = alpm_list_add(*l, t->data);
}

static inline void
list_artix_packages(struct global *g)
{
    alpm_list_t *artix_pkgs = list_pkgs(g->artix_h);
    if (!artix_pkgs)
        return;

    alpm_list_t *rbase = NULL;
    for (alpm_list_t *pkg = artix_pkgs; pkg; pkg = pkg->next) {
        alpm_pkg_t *artixpkg = pkg->data;
        if (alpm_list_find_str(rbase, alpm_pkg_get_base(artixpkg)))
            continue;

        if (g->packagers && !matching_packager(g->packagers, alpm_pkg_get_packager(artixpkg)))
            continue;

        alpm_pkg_t **archpkgs = get_arch_pkgs(g, artixpkg);
        if (!archpkgs[0]) {
            print_formatted_package(NULL, artixpkg);
            rbase = alpm_list_add(rbase, (void *)alpm_pkg_get_base(artixpkg));
        }
        free(archpkgs);
    }

    alpm_list_free(artix_pkgs);
    alpm_list_free(rbase);
}

static int needs_pkgver_rename(const char *pkgbase)
{
    for (int i = 0; pkgver_map[i].pkgbase; ++i) {
        if (strcmp(pkgver_map[i].pkgbase, pkgbase) == 0)
            return i;
    }
    return -1;
}

__attribute_warn_unused_result__ static inline char *
version_normalizer(alpm_pkg_t *p, bool arch_mapping)
{
    if (arch_mapping) {
        // Map one string to another in pkgver (e.g. arch to artix)
        int index = needs_pkgver_rename(alpm_pkg_get_base(p));
        if (index >= 0) {
            char pver[1024]      = {0};
            char *pkgver         = strdup(alpm_pkg_get_version(p));
            char *dotchar        = strrchr(pkgver, '.');
            if (dotchar) {
                struct str_arr *split = split_string(dotchar, (char *)pkgver_map[index].find);
                snprintf(pver, sizeof(pver), "%s%s%s", pkgver, pkgver_map[index].replace, split->arr[split->len - 1]);
                destroy_str_arr(split);
            }
            free(pkgver);
            return strdup(pver);
        } else {
            return strdup(alpm_pkg_get_version(p));
        }
    } else {
        char *pver = strdup(alpm_pkg_get_version(p));
        if (!pver)
            return NULL;

        const char *prel = strrchr(pver, '-');
        if (prel) {
            char *psub = strrchr(prel, '.');
            if (psub)
                *psub = 0;
        }

        return pver;
    }
}

static inline void
list_moves(struct global *g)
{
    if (!g->arch_h || !g->artix_h)
        return;

    alpm_list_t *artix_staging = list_pkgs_suffix(g->artix_h, "goblins");

    alpm_list_t *rbase = NULL;
    for (alpm_list_t *l = artix_staging; l; l = l->next) {
        alpm_pkg_t *artixpkg = l->data;
        if (alpm_list_find_str(rbase, alpm_pkg_get_base(artixpkg)))
            continue;

        if (g->filter && !is_pkg_in_tree(g->artix_tree_dir, alpm_pkg_get_base(artixpkg)))
            continue;

        if (g->packagers && !matching_packager(g->packagers, alpm_pkg_get_packager(artixpkg)))
            continue;

        alpm_pkg_t *archpkg = get_arch_pkg_suffix(g->arch_h, artixpkg, "testing");
        if (!archpkg)
            archpkg = get_arch_pkg_suffix(g->arch_h, artixpkg, NULL);

        if (!archpkg) {
            archpkg = get_arch_pkg_suffix(g->arch_h, artixpkg, "staging");
            if (archpkg)
                continue;
        }

        if (archpkg) {
            char *artixver = version_normalizer(artixpkg, false);
            char *archver  = version_normalizer(archpkg, true);
            bool skip = false;

            if (g->package_loose_moves) {
                if (alpm_pkg_vercmp(archver, artixver) < 0)
                    skip = true;
            } else {
                if (alpm_pkg_vercmp(archver, artixver) != 0)
                    skip = true;
            }

            free(artixver);
            free(archver);

            if (skip)
                continue;
        }

        print_formatted_package(archpkg, artixpkg);
        write_move(g, archpkg, artixpkg);
        rbase = alpm_list_add(rbase, (void *)alpm_pkg_get_base(artixpkg));
    }

    alpm_list_free(artix_staging);
    artix_staging = NULL;
    alpm_list_free(rbase);
    rbase = NULL;

    alpm_list_t *artix_testing = list_pkgs_suffix(g->artix_h, "gremlins");

    for (alpm_list_t *l = artix_testing; l; l = l->next) {
        alpm_pkg_t *artixpkg = l->data;
        if (alpm_list_find_str(rbase, alpm_pkg_get_base(artixpkg)))
            continue;

        if (g->filter && !is_pkg_in_tree(g->artix_tree_dir, alpm_pkg_get_base(artixpkg)))
            continue;

        if (g->packagers && !matching_packager(g->packagers, alpm_pkg_get_packager(artixpkg)))
            continue;

        alpm_pkg_t *archpkg = get_arch_pkg_suffix(g->arch_h, artixpkg, NULL);

        if (!archpkg) {
            archpkg = get_arch_pkg_suffix(g->arch_h, artixpkg, "testing");
            if (archpkg)
                continue;
        }

        if (archpkg) {
            char *artixver = version_normalizer(artixpkg, false);
            char *archver  = version_normalizer(archpkg, true);
            bool skip = false;

            if (g->package_loose_moves) {
                if (alpm_pkg_vercmp(archver, artixver) < 0)
                    skip = true;
            } else {
                if (alpm_pkg_vercmp(archver, artixver) != 0)
                    skip = true;
            }
            free(artixver);
            free(archver);

            if (skip)
                continue;
        }

        print_formatted_package(archpkg, artixpkg);
        write_move(g, archpkg, artixpkg);
        rbase = alpm_list_add(rbase, (void *)alpm_pkg_get_base(artixpkg));
    }
    alpm_list_free(artix_testing);
    alpm_list_free(rbase);
}

__attribute_warn_unused_result__ static inline alpm_list_t *
list_pkgs(alpm_handle_t *h)
{
    alpm_list_t *r = NULL;
    for (alpm_list_t *e = alpm_get_syncdbs(h); e; e = e->next) {
        alpm_list_t *t = alpm_db_get_pkgcache(e->data);
        if (!t) {
            if (alpm_errno(h) == ALPM_ERR_OK)
                continue;
            fprintf(stderr, "Failed to list packages: %s\n", ALPM_ERROR(h));
            return NULL;
        }
        for (; t; t = t->next)
            r = alpm_list_add(r, t->data);
    }
    return r;
}

__attribute_warn_unused_result__ static inline alpm_list_t *
list_pkgs_suffix(alpm_handle_t *restrict h, const char *restrict suffix)
{
    alpm_list_t *r = NULL;
    for (alpm_list_t *e = alpm_get_syncdbs(h); e; e = e->next) {
        alpm_db_t *db = e->data;

        const char *dbname = alpm_db_get_name(db);
        const char *p      = strrchr(dbname, '-');

        if (!p) {
            if (suffix)
                continue;
        } else if (!suffix)
            continue;
        else if (strcmp(p + 1, suffix))
            continue;
        list_add_pkgs(h, db, &r);
    }
    return r;
}

static inline bool
is_pkg_in_tree(const char *tree, const char *pkgbase)
{
    char path[1024] = {0};
    snprintf(path, sizeof(path), "%s/%s", tree, pkgbase);

    struct stat s;
    if (stat(path, &s) == -1) {
        if (errno == ENOENT)
            return false;

        perror("stat");
        return false;
    }

    return true;
}

static inline void
list_upgrades(struct global *g)
{
    alpm_list_t *artix_pkgs = list_pkgs(g->artix_h);
    if (!artix_pkgs)
        return;

    alpm_list_t *rbase = NULL;
    for (alpm_list_t *pkg = artix_pkgs; pkg; pkg = pkg->next) {
        alpm_pkg_t *artixpkg = pkg->data;
        if (alpm_list_find_str(rbase, alpm_pkg_get_base(artixpkg)))
            continue;

        if (g->filter && !is_pkg_in_tree(g->artix_tree_dir, alpm_pkg_get_base(artixpkg)))
            continue;

        alpm_pkg_t **archpkgs = get_arch_pkgs(g, artixpkg);
        if (!archpkgs[0])
            goto finish;
        int index = 0;
        while (archpkgs[index] != NULL) {
            char *artixver = version_normalizer(artixpkg, false);
            char *archver  = version_normalizer(archpkgs[index], true);

            if (!g->packagers || matching_packager(g->packagers, alpm_pkg_get_packager(artixpkg))) {
                switch (alpm_pkg_vercmp(archver, artixver)) {
                case 1:
                    if (g->package_upgrades) {
                        print_formatted_package(archpkgs[index], artixpkg);
                        write_upgrade(g, archpkgs[index], artixpkg);
                    }
                    break;
                case -1:
                    // Only show downgrades if the repo level is the same.
                    if (g->package_downgrades && same_repo(archpkgs[index], artixpkg)) {
                        print_formatted_package(archpkgs[index], artixpkg);
                        write_upgrade(g, archpkgs[index], artixpkg);
                    }
                    break;
                }
            }

            free(artixver);
            free(archver);

            ++index;
        }
        rbase = alpm_list_add(rbase, (void *)alpm_pkg_get_base(artixpkg));
finish:
        free(archpkgs);
    }

    alpm_list_free(artix_pkgs);
    alpm_list_free(rbase);
}

static bool
matching_packager(struct str_arr *packagers, const char *packager)
{
    char *packager_name = strtok((char *)packager, "<");
    int len = strlen(packager_name);
    if (isspace(packager_name[len - 1]))
        packager_name[len - 1] = 0;
    for (size_t i = 0; i < packagers->len; ++i) {
        if (strcmp(packagers->arr[i], packager_name) == 0)
            return true;
    }
    return false;
}

static inline void
print_formatted_package(alpm_pkg_t *restrict archpkg, alpm_pkg_t *restrict artixpkg)
{
    char *packager_name = strtok((char *)alpm_pkg_get_packager(artixpkg), "<");
    int len = strlen(packager_name);
    if (isspace(packager_name[len - 1]))
        packager_name[len - 1] = 0;
    printf("%-32s %-16s %-32s %-16s %-32s %-32s\n", alpm_pkg_get_base(artixpkg),
           alpm_db_get_name(alpm_pkg_get_db(artixpkg)), alpm_pkg_get_version(artixpkg),
           archpkg ? alpm_db_get_name(alpm_pkg_get_db(archpkg)) : "N/A",
           archpkg ? alpm_pkg_get_version(archpkg) : "N/A", packager_name);
}

static void
print_header(void)
{
    printf("\n%-32s %-16s %-32s %-16s %-32s %-32s\n", "Package basename", "Artix repo",
           "Artix version", "Arch repo", "Arch version", "Packager");
}

static inline int
register_dbs(alpm_handle_t *h, struct str_arr *repos, const char *url_template, bool no_sync)
{
    if (!h || !repos || !url_template)
        return 0;

    for (unsigned int i = 0; i < repos->len; ++i) {
        const char *repo = repos->arr[i];
        alpm_db_t *db    = alpm_register_syncdb(h, repo, 0);
        if (!db) {
            fprintf(stderr, "Failed to register %s db: %s\n", repo, ALPM_ERROR(h));
            return 0;
        }

        const int flags =
            no_sync ? ALPM_DB_USAGE_SEARCH : ALPM_DB_USAGE_SYNC | ALPM_DB_USAGE_SEARCH;
        if (alpm_db_set_usage(db, flags)) {
            fprintf(stderr, "Failed to set db usage for %s: %s\n", repo, ALPM_ERROR(h));
            return 0;
        }

        char url[1024] = {0};
        snprintf(url, sizeof(url), url_template, repo);
        if (alpm_db_add_server(db, url)) {
            fprintf(stderr, "Failed to set server for %s: %s\n", repo, ALPM_ERROR(h));
            return 0;
        }
    }

    return 1;
}

static bool
same_repo(alpm_pkg_t *archpkg, alpm_pkg_t *artixpkg)
{
    const char *archdb  = alpm_db_get_name(alpm_pkg_get_db(archpkg));
    const char *artixdb = alpm_db_get_name(alpm_pkg_get_db(artixpkg));

    const char *arch_suffix  = strrchr(archdb, '-');
    const char *artix_suffix = strrchr(artixdb, '-');

    if (!arch_suffix && !artix_suffix)
        return true;

    if ((!arch_suffix && artix_suffix) || (arch_suffix && !artix_suffix))
        return false;

    if (arch_suffix)
        arch_suffix += 1;
    if (artix_suffix)
        artix_suffix += 1;

    return (strcmp(arch_suffix, "staging") == 0 && strcmp(artix_suffix, "goblins") == 0) ||
           (strcmp(arch_suffix, "testing") == 0 && strcmp(artix_suffix, "gremlins") == 0);
}

static void sigint_handler(int signal)
{
    switch (signal) {
    case SIGINT:
    case SIGTERM:
        if (dbpath[0]) {
            char lock_file[1024] = {0};
            snprintf(lock_file, sizeof(lock_file), "%s/%s", dbpath, "db.lck");
            remove(lock_file);
        }
        exit(EXIT_FAILURE);
        break;
    default:
        break;
    }
}

static void
setup_signal_handler(void)
{
    struct sigaction act = {0};
    act.sa_handler = &sigint_handler;
    sigaction(SIGINT, &act, NULL);
}

static inline int
syncdbs(alpm_handle_t *h)
{
    if (alpm_db_update(h, alpm_get_syncdbs(h), 0) < 0) {
        fprintf(stderr, "Failed to sync databases: %s\n", ALPM_ERROR(h));
        return 0;
    }
    return 1;
}

static inline void
write_move(struct global *g, alpm_pkg_t *archpkg, alpm_pkg_t *artixpkg)
{
    if (!g->output || !archpkg || !artixpkg)
        return;

    const char *src     = alpm_db_get_name(alpm_pkg_get_db(artixpkg));
    char *dst           = (char *)arch2artix(alpm_db_get_name(alpm_pkg_get_db(archpkg)));
    const char *pkgbase = alpm_pkg_get_base(artixpkg);

    // Special handling for galaxy repos
    if (strncmp(src, "galaxy", 6) == 0) {
        char new_dst[512]  = {0};
        const char *suffix = strrchr(dst, '-');
        if (suffix) {
            suffix += 1;
            snprintf(new_dst, sizeof(new_dst), "%s-%s", "galaxy", suffix);
        } else {
            suffix = "";
            snprintf(new_dst, sizeof(new_dst), "%s", "galaxy");
        }
        add_to_move_lists(g, src, new_dst, pkgbase);
    } else {
        add_to_move_lists(g, src, dst, pkgbase);
    }
}

static void
write_script(struct global *g)
{
    if (!g->write_moves[0] && !g->write_upgrades[0])
        return;
    fprintf(g->script, "#!/bin/sh\n");
    for (int i = 0; i < g->write_upgrades_lists; ++i) {
        fprintf(g->script, "artixpkg repo add -p ");
        for (alpm_list_t *upgrades = g->write_upgrades[i]; upgrades; upgrades = upgrades->next) {
            struct str_arr *entry = upgrades->data;
            fprintf(g->script, "%s ", entry->arr[0]);
        }
        fprintf(g->script, "\n");
    }
    for (int i = 0; i < g->write_moves_lists; ++i) {
        fprintf(g->script, "artixpkg repo move -p ");
        for (alpm_list_t *moves = g->write_moves[i]; moves; moves = moves->next) {
            const char *str = moves->data;
            fprintf(g->script, "%s ", str);
        }
        fprintf(g->script, "\n");
    }
}

static inline void
write_upgrade(struct global *g, alpm_pkg_t *archpkg, alpm_pkg_t *artixpkg)
{
    if (!g->output || !archpkg || !artixpkg)
        return;

    const char *src     = alpm_db_get_name(alpm_pkg_get_db(artixpkg));
    char *dst           = (char *)arch2artix(alpm_db_get_name(alpm_pkg_get_db(archpkg)));
    const char *pkgbase = alpm_pkg_get_base(artixpkg);
    char *version       = version_normalizer(artixpkg, false);

    // Special handling for galaxy repos
    if (strncmp(src, "galaxy", 6) == 0) {
        char new_dst[512]  = {0};
        const char *suffix = strrchr(dst, '-');
        if (suffix) {
            suffix += 1;
            snprintf(new_dst, sizeof(new_dst), "%s-%s", "galaxy", suffix);
        } else {
            suffix = "";
            snprintf(new_dst, sizeof(new_dst), "%s", "galaxy");
        }
        add_to_upgrades_lists(g, new_dst, pkgbase, version);
    } else {
        add_to_upgrades_lists(g, dst, pkgbase, version);
    }
}

static void
write_yaml(struct global *g)
{
    // Only upgrades are written.
    if (!g->write_upgrades[0])
        return;
    for (int i = 0; i < g->write_upgrades_lists; ++i) {
        bool repo_name = false;
        for (alpm_list_t *upgrades = g->write_upgrades[i]; upgrades; upgrades = upgrades->next) {
            struct str_arr *entry = upgrades->data;
            if (!repo_name) {
                fprintf(g->yaml, "%s:\n", entry->arr[0]);
                repo_name = true;
            } else {
                fprintf(g->yaml, "  %s: %s\n", entry->arr[0], entry->arr[1]);
            }
        }
    }
}

int
main(int argc, char **argv)
{
    struct global *g = zalloc(sizeof(struct global));
    if (!g) {
        perror("Failed to allocate memory");
        return 1;
    }

    int c          = 0;
    int ret        = 0;
    bool arguments = false;
    while (1) {
        int option_id = 0;
        c             = getopt_long(argc, argv, "ab:dfhlmnp:uw:y:", long_options, &option_id);
        if (c == -1)
            break;
        arguments = true;
        switch (c) {
        case 'a':
            g->artix_packages = true;
            break;
        case 'b':
            g->repo_blacklist = split_string(optarg, ",");
            break;
        case 'd':
            g->package_downgrades = true;
            break;
        case 'f':
            g->filter = true;
            break;
        case 'h':
            fputs(help_message, stderr);
            return 0;
        case 'l':
            g->package_loose_moves = true;
            break;
        case 'm':
            g->package_moves = true;
            break;
        case 'n':
            g->no_sync = true;
            break;
        case 'p':
            g->packagers = split_string(optarg, ",");
            break;
        case 'u':
            g->package_upgrades = true;
            break;
        case 'w':
            g->output = true;
            g->script = fopen(optarg, "w");
            if (!g->script) {
                perror("Unable to open output file");
                ret = 1;
                goto err;
            }
            break;
        case 'y':
            g->output = true;
            g->yaml = fopen(optarg, "w");
            if (!g->yaml) {
                perror("Unable to open output file");
                ret = 1;
                goto err;
            }
            break;
        case '?':
            ret = 1;
            goto err;
        }
    }

    if (!arguments || !have_operation(g)) {
        fputs(help_message, stderr);
        ret = 1;
        goto err;
    }

    char *home       = getenv("HOME");
    if (home)
        snprintf(dbpath, sizeof(dbpath), "%s/.cache/artix-checkupdates", home);
    else
        snprintf(dbpath, sizeof(dbpath), "/tmp/cache.artix-checkupdates");

    if (mkdir(dbpath, S_IRWXU | S_IRGRP | S_IXGRP)) {
        if (errno != EEXIST) {
            perror("Failed to create cache database directory");
            ret = 1;
            goto err;
        }
    }

    char home_cfgfile[512] = {0};
    char *global_cfgfile   = "/etc/artix-checkupdates/config";
    snprintf(home_cfgfile, sizeof(home_cfgfile), "%s/.config/artix-checkupdates/config", home);

    FILE *file = fopen(home_cfgfile, "r");
    if (!file) {
        file = fopen(global_cfgfile, "r");
        if (!file) {
            perror("Failed to open config file");
            ret = 1;
            goto err;
        }
    }

    char artix_url[512]      = {0};
    char arch_url[512]       = {0};
    char artix_repoline[512] = {0};
    char arch_repoline[512]  = {0};

    char *artix_mirror_env   = getenv("ARTIX_MIRROR");
    char *arch_mirror_env    = getenv("ARCH_MIRROR");
    char *artix_repoline_env = getenv("ARTIX_REPOS");
    char *arch_repoline_env  = getenv("ARCH_REPOS");
    char *artix_tree_dir_env = getenv("ARTIX_TREE_DIR");

    if (artix_mirror_env && artix_mirror_env[0])
        snprintf(artix_url, sizeof(artix_url), "%s", artix_mirror_env);
    if (arch_mirror_env && arch_mirror_env[0])
        snprintf(arch_url, sizeof(arch_url), "%s", arch_mirror_env);
    if (artix_repoline_env && artix_repoline_env[0])
        snprintf(artix_repoline, sizeof(artix_repoline), "%s", artix_repoline_env);
    if (arch_repoline_env && arch_repoline_env[0])
        snprintf(arch_repoline, sizeof(arch_repoline), "%s", arch_repoline_env);
    if (artix_tree_dir_env && artix_tree_dir_env[0])
        snprintf(g->artix_tree_dir, sizeof(g->artix_tree_dir), "%s", artix_tree_dir_env);

    char line[512] = {0};

    while (fgets(line, sizeof(line), file)) {
        if (line[0] == '#')
            continue;
        if (!artix_url[0] && strncmp("ARTIX_MIRROR=", line, 13) == 0)
            read_config_line(artix_url, line, 512);
        if (!arch_url[0] && strncmp("ARCH_MIRROR=", line, 12) == 0)
            read_config_line(arch_url, line, 512);
        if (!artix_repoline[0] && strncmp("ARTIX_REPOS=", line, 12) == 0)
            read_config_line(artix_repoline, line, 512);
        if (!arch_repoline[0] && strncmp("ARCH_REPOS=", line, 11) == 0)
            read_config_line(arch_repoline, line, 512);
        if (!g->artix_tree_dir[0] && strncmp("ARTIX_TREE_DIR=", line, 11) == 0)
            read_config_line(g->artix_tree_dir, line, 512);
    }
    fclose(file);

    if (!*artix_url) {
        fprintf(stderr, "Unable to get an artix mirror url!\n");
        ret = 1;
        goto err;
    }

    if (!*arch_url) {
        fprintf(stderr, "Unable to get an arch mirror url!\n");
        ret = 1;
        goto err;
    }

    if (!*artix_repoline) {
        fprintf(stderr, "Unable to get a list of artix repos!\n");
        ret = 1;
        goto err;
    }

    if (!*arch_repoline) {
        fprintf(stderr, "Unable to get a list of arch repos!\n");
        ret = 1;
        goto err;
    }

    if (!*g->artix_tree_dir)
        snprintf(g->artix_tree_dir, sizeof(g->artix_tree_dir), "%s/artools-workspace/artixlinux", home);

    setup_signal_handler();

    g->artix_h = create_handle(dbpath);
    if (!g->artix_h) {
        ret = 2;
        goto err;
    }

    g->artix_repos = split_string(artix_repoline, ",");
    if (!register_dbs(g->artix_h, g->artix_repos, artix_url, g->no_sync)) {
        ret = 3;
        goto err;
    }

    g->arch_h = create_handle(dbpath);
    if (!g->arch_h) {
        ret = 4;
        goto err;
    }

    g->arch_repos = split_string(arch_repoline, ",");
    if (!register_dbs(g->arch_h, g->arch_repos, arch_url, g->no_sync)) {
        ret = 5;
        goto err;
    }

    if (!g->no_sync) {
        syncdbs(g->artix_h);
        syncdbs(g->arch_h);
    }

    if (g->script) {
        g->write_moves    = zalloc(sizeof(alpm_list_t));
        g->write_upgrades = zalloc(sizeof(alpm_list_t));
    }
    if (g->package_downgrades || g->package_upgrades) {
        print_header();
        list_upgrades(g);
    }

    if (g->package_moves) {
        print_header();
        list_moves(g);
    }

    if (g->artix_packages) {
        print_header();
        list_artix_packages(g);
    }

    if (g->script)
        write_script(g);

    if (g->yaml)
        write_yaml(g);

err:
    global_destroy(g);
    return ret;
}

// vim: ts=4 sw=4 et
