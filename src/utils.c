// artix-checkupdates
//
// Copyright (C) 2023 Artix Dev team <artix-dev@artixlinux.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

void *
zalloc(size_t s);

void
destroy_str_arr(struct str_arr *split)
{
    for (size_t i = 0; i < split->len; ++i)
        free(split->arr[i]);
    free(split->arr);
    free(split);
}

void
read_config_line(char *str, char *line, int size)
{
    char *token                 = strtok(line, "=");
    token                       = strtok(NULL, "=");
    token[strcspn(token, "\n")] = 0;
    snprintf(str, size, "%s", token);
}

static struct str_arr
*create_str_arr_va(int len, va_list va)
{
    struct str_arr *arr = zalloc(sizeof(struct str_arr));
    arr->arr = zalloc(len * sizeof(char *));
    arr->len = len;

    for (int i = 0; i < len; ++i)
        arr->arr[i] = strdup(va_arg(va, char *));

    va_end(va);
    return arr;
}

__attribute_warn_unused_result__ struct str_arr
*create_str_arr(int len, ...)
{
    va_list va;
    va_start(va, len);
    return create_str_arr_va(len, va);
}

__attribute_warn_unused_result__ struct str_arr
*split_string(char *str, char *delim)
{
    size_t len = 1;
    for (size_t i = 0; i < strlen(str); ++i) {
        if (str[i] == *delim)
            ++len;
    }
    struct str_arr *split = zalloc(sizeof(struct str_arr));
    split->arr            = zalloc(len * sizeof(char *));
    split->len            = len;

    int i       = 0;
    char *token = strtok(str, delim);
    while (token != NULL) {
        split->arr[i]                              = strdup(token);
        split->arr[i][strcspn(split->arr[i], "\n")] = 0;
        token                                     = strtok(NULL, delim);
        ++i;
    }
    return split;
}

void *
zalloc(size_t s)
{
    return calloc(1, s);
}
