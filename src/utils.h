// artix-checkupdates
//
// Copyright (C) 2023 Artix Dev team <artix-dev@artixlinux.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

struct str_arr {
    char **arr;
    size_t len;
};

void destroy_str_arr(struct str_arr *split);
void read_config_line(char *str, char *line, int size);
struct str_arr *split_string(char *str, char *delim);
struct str_arr *create_str_arr(int len, ...);
void *zalloc(size_t s);
