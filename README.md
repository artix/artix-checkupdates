artix-checkupdates
=============

#### Configuration

By default, the config file will first be read from `$HOME/.config/artix-checkupdates/config`.
If this is not found, then it will fallback to `/etc/artix-checkupdates/config`.

Four variables are required in the config and one is optional. For the urls, %s
denotes the repo. For specifying repo names, it must be a comma separated
string.
```
ARTIX_MIRROR=https://artixmirror.com/%s/os/x86_64
ARCH_MIRROR=https://archmirror.com/%s/os/x86_64
ARTIX_REPOS=system-goblins,world-goblins,lib32-goblins,system-gremlins,world-gremlins,lib32-gremlins,system,world,lib32
ARCH_REPOS=core-staging,extra-staging,multilib-staging,core-testing,extra-testing,multilib-testing,core,extra,multilib

# Optional: for use with -f flag
ARTIX_TREE_DIR=/home/artix/artools-workspace/artixlinux
```
Missing `ARTIX_MIRROR`, `ARCH_MIRROR`, `ARTIX_REPOS`, or `ARCH_REPOS` will result in an error.

You can also override the config file by setting `ARTIX_MIRROR`, `ARCH_MIRROR`,
    `ARTIX_REPOS`,`ARCH_REPOS` and `ARTIX_TREE_DIR` as environment variables.
