CPPFLAGS := -D_POSIX_C_SOURCE=200809L -D_FORTIFY_SOURCE=3
CFLAGS   := -O2 -std=c17 -pedantic -Wall -Wextra -Werror -Wformat -Wformat-security -Werror=format-security
LDFLAGS  := -lalpm

V=0.10.7
PREFIX ?= /usr
SYSCONFDIR = /etc
BINDIR = $(PREFIX)/bin
LICENSEDIR = $(PREFIX)/share/licenses
DIRMODE = -dm0755
FMODE = -m0644
EMODE = -m0755

SRCS := $(wildcard src/*.c)
OBJS := $(patsubst %.c,%.o,$(SRCS))
CONF := config
BIN  := artix-checkupdates
LICENSE := LICENSE

default: $(BIN)

$(BIN): $(OBJS)
	$(CC)    -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) -c -o $@ $^ $(CPPFLAGS) $(CFLAGS)

clean-objs: FORCE
	@$(RM) -v $(OBJS)

clean: clean-objs
	@$(RM) -v $(BIN)

format: FORCE
	@clang-format -i --verbose $(SRCS)

FORCE:

install:
	install $(DIRMODE) $(DESTDIR)$(BINDIR)
	install $(EMODE) $(BIN) $(DESTDIR)$(BINDIR)
	install $(DIRMODE) $(DESTDIR)$(SYSCONFDIR)/artix-checkupdates
	install $(FMODE) $(CONF) $(DESTDIR)$(SYSCONFDIR)/artix-checkupdates/config
	install $(DIRMODE) $(DESTDIR)$(LICENSEDIR)/artix-checkupdates
	install $(FMODE) $(LICENSE) $(DESTDIR)$(LICENSEDIR)/artix-checkupdates
